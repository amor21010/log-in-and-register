package com.aboesmail.omar.loggin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import static android.view.View.VISIBLE;

public class LoginActivity extends AppCompatActivity {
    TextInputLayout emailLogIn;
    TextInputLayout passLogin;
    Button logConfirm;
    Button reg;
    SessonManger sessonManger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailLogIn = findViewById(R.id.log_email);
        passLogin = findViewById(R.id.log_pass);
        logConfirm = findViewById(R.id.confirm);
        reg = findViewById(R.id.reg);

        sessonManger = new SessonManger(this);


        logConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmInput()) {
                    Intent intent = new Intent(LoginActivity.this, info.class);
                    startActivity(intent);
                }
            }
        });

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, Register.class);
                startActivity(intent);
            }
        });

    }


    private boolean Validatrepass() {
        String srepass = String.valueOf(passLogin.getEditText().getText());
        String cachPass = sessonManger.getUserData().get("pass").toString();

        if (srepass.isEmpty()) {
            passLogin.setError("this field is required");
            return false;
        } else if (!srepass.equals(cachPass)) {
            passLogin.setError("passwords doesn't match");
            return false;
        } else {
            passLogin.setError(null);

            return true;
        }
    }


    private boolean ValidateEmail() {
        String semail = String.valueOf(emailLogIn.getEditText().getText());
        String cachEmail = sessonManger.getUserData().get("email").toString().toLowerCase();
        if (semail.isEmpty()) {
            emailLogIn.setError("this field is required");

            return false;
        } else if (!semail.equals(cachEmail)) {
            emailLogIn.setError("email doesn't match");

            return false;
        } else {
            emailLogIn.setError(null);
            return true;
        }
    }

    public boolean confirmInput() {
        if (!ValidateEmail() | !Validatrepass()) {
            reg.setVisibility(VISIBLE);
            return false;
        } else {

            return true;
        }

    }
}
