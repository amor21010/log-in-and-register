package com.aboesmail.omar.loggin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Pattern;


public class Register extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 1;

    Button button;
    TextInputLayout email;
    TextInputLayout phone;
    TextInputLayout pass;
    TextInputLayout repass;
    TextInputLayout name;
    TextView add;
    ImageView im;
    Uri selectedImageURI;
    SessonManger sessonManger;

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
//                    "(?=.*[a-zA-Z])" +      //any letter
//                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 4 characters
                    "$");
    private static final Pattern NAME_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
//                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
//                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 4 characters
                    "$");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        button = findViewById(R.id.regbtn);
        email = findViewById(R.id.editemail);
        phone = findViewById(R.id.phone);
        pass = findViewById(R.id.editpass);
        repass = findViewById(R.id.confirmpass);
        name = findViewById(R.id.editname);
        add = findViewById(R.id.add);
        im = findViewById(R.id.im);
        sessonManger = new SessonManger(this);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }


        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmInput()) {
                    sessonManger.userEntry(
                            selectedImageURI.toString(),
                            name.getEditText().getText().toString(),
                            email.getEditText().getText().toString(),
                            phone.getEditText().getText().toString(),
                            pass.getEditText().getText().toString(),
                            repass.getEditText().getText().toString()
                    );
                    String input = "name : " + sessonManger.getUserData().get("name").toString() + "\n" +
                            "phone : " + sessonManger.getUserData().get("phone").toString() + "\n" +
                            "Email : " + sessonManger.getUserData().get("email").toString() + "\n" +
                            "password : " + sessonManger.getUserData().get("pass").toString() + "\n";

                    Toast.makeText(Register.this, input, Toast.LENGTH_SHORT).show();
                    submitinf();
                }
            }
        });


    }

    private boolean ValidateEmail() {
        String semail = String.valueOf(email.getEditText().getText());
        if (semail.isEmpty()) {
            email.setError("this field is required");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(semail).matches()) {
            email.setError("enter a valid email address");
            return false;
        } else {
            email.setError(null);
            return true;
        }
    }

    private boolean ValidatPhone() {
        String sphone = String.valueOf(phone.getEditText().getText());
        if (sphone.isEmpty()) {
            phone.setError("this field is required");
            return false;
        } else if (sphone.length() != 11) {
            phone.setError("Name must 11 number");
            return false;
        } else {
            phone.setError(null);
            return true;
        }
    }

    private boolean ValidatName() {
        String sname = String.valueOf(name.getEditText().getText());
        if (sname.isEmpty()) {
            name.setError("this field is required");
            return false;
        } else if (sname.length() > 15) {
            name.setError("Name must be under 15 character");
            return false;
        } else if (!NAME_PATTERN.matcher(sname).matches()) {
            name.setError("Name must be 6 character only or above");
            return false;
        } else {
            name.setError(null);
            return true;
        }

    }

    private boolean ValidatPass() {
        String spass = String.valueOf(pass.getEditText().getText());
        if (spass.isEmpty()) {
            pass.setError("this field is required");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(spass).matches()) {
            pass.setError("Must must be 6 char or above");
            return false;
        } else {
            pass.setError(null);
            return true;
        }
    }

    private boolean Validatrepass() {
        String srepass = String.valueOf(repass.getEditText().getText());
        if (srepass.isEmpty()) {
            repass.setError("this field is required");
            return false;
        } else if (!srepass.equals(String.valueOf(pass.getEditText().getText()))) {
            repass.setError("passwords doesn't match");
            return false;
        } else {
            repass.setError(null);
            return true;
        }
    }

    public boolean image() {
        if (selectedImageURI == null) {
            Toast.makeText(Register.this, "please select a photo", Toast.LENGTH_SHORT).show();
            return false;
        } else return true;
    }

    public boolean confirmInput() {
        if (!image() | !ValidateEmail() | !ValidatName() | !ValidatPhone() | !ValidatPass() | !Validatrepass()) {
            return false;
        } else {

            return true;
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void submitinf() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                selectedImageURI = data.getData();

                im.setImageURI(selectedImageURI);

            }

        }
    }

}



