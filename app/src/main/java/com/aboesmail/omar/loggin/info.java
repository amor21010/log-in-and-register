package com.aboesmail.omar.loggin;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class info extends AppCompatActivity {
    TextView name;
    TextView phone;
    TextView email;
    TextView pass;
    ImageView image;
    SessonManger sessonManger;
    Button loggout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        image = findViewById(R.id.imageView);
        name = findViewById(R.id.msgname);
        phone = findViewById(R.id.msgphone);
        email = findViewById(R.id.msgemail);
        pass = findViewById(R.id.msgpass);
        loggout = findViewById(R.id.lgout);
        sessonManger = new SessonManger(this);


        image.setImageURI(Uri.parse(sessonManger.getUserData().get("photo").toString()));
        name.setText(sessonManger.getUserData().get("name").toString());
        phone.setText(sessonManger.getUserData().get("phone").toString());
        email.setText(sessonManger.getUserData().get("email").toString());
        pass.setText(sessonManger.getUserData().get("pass").toString());

        loggout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(info.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
