package com.aboesmail.omar.loggin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessonManger {


    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public SessonManger(Context context) {
        sharedPreferences = context.getSharedPreferences("new app", context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void Lggin(boolean islggin) {
        editor.putBoolean("loggin", false);
    }

    public boolean isLoggin() {
        return sharedPreferences.getBoolean("loggin", false);
    }


    public void userEntry(String photo, String name, String email, String phone, String pass, String repass) {
        editor.putString("photo", photo);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.putString("phone", phone);
        editor.putString("pass", pass);
        editor.putString("repass", repass);
        editor.commit();

    }

    public HashMap<String, Object> getUserData() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("photo", sharedPreferences.getString("photo", "R.drawable.ic_launcher_background"));
        map.put("name", sharedPreferences.getString("name", null));
        map.put("email", sharedPreferences.getString("email", null));
        map.put("phone", sharedPreferences.getString("phone", null));
        map.put("pass", sharedPreferences.getString("pass", null));
        map.put("repass", sharedPreferences.getString("repass", null));


        return map;
    }
}


